/* 
**  LCbrandSlider.js
**  version : 0.2.3
**  2016-03-22
**  Dru
*/


(function ( $ ) {
    // Init Slider and insert HTML
    $.fn.LCbrandSlider = function(lcbrands) {
        var container = $(this),
            pID = $('body').attr("class").match(/(^|[ ])categorypath-([\w-]*)\b/), // id will be in pos [2]
            configURL='//d11iw3n3gaf421.cloudfront.net/LCBrandSlider/LCbrandSlider.config.json';
            
             // DEVIL
            //configURL='//media.lac-devel.commer.com/LCBrandSlider/LCbrandSlider.config.json';

        function fetchJSON(){ // retrieve JSON file containing pages & brands match
            return $.ajax({
                type:'GET',
                dataType: "jsonp", //json file not hosted on same server (cached), the jQuery way
                url: configURL,
                jsonpCallback: "LCBjson", // Function name used in the json file...
                success: function(data){
                    var LCBsliders = data.sliders;
                    brands = false;
                    for (var i = 0; i < LCBsliders.length; i++) {
                        if ( LCBsliders[i].id_fr == pID[2] || LCBsliders[i].id_en == pID[2] ) {
                            brands = LCBsliders[i].brands;
                            return(brands);
                        }
                    }
                }
            }); 
        };

        fetchJSON().done(function(result){
            if ( brands === false ) {  // Check if brands exist and stop execution if false
                return; }

            var brandsFilter = ".nav_attribute .app_webattribute_2 li.", //Magento Layered nav brand
                mediaURL = "https://d3ejnbvvoe0sdn.cloudfront.net/media/Marques/LCbrandSlider/100x100/",
                sliderHTML = '<div class="lc-brand-slider"><ul class="gal"></ul><div class="gal-nav"><div class="gal-move" id="move-prev" ></div><div class="gal-move" id="move-next" ></div></div></div>';

            if ( container.length == 0 ) { // check if element exists. If not, create it
                LCclassName = container['selector'].match(/[\.\#]{1}?(\w*)/)[1]; // Clean and return classname or id from jQuery object selector
                $('.page-title').after('<div class="' + LCclassName + ' lc-brand-slider-container"></div>');
                container = $(".lc-brand-slider-container");
            } else {
                container.addClass("lc-brand-slider-container");
            }
            
            container.append(sliderHTML);

            var gallery = $(".lc-brand-slider ul.gal", container); 

            accentsTidy = function(s){
                var r = s.toLowerCase();
                non_asciis = {'a': '[àáâãäå]', 'ae': 'æ', 'c': 'ç', 'e': '[èéêë]', 'i': '[ìíîï]', 'n': 'ñ', 'o': '[òóôõö]', 'oe': 'œ', 'u': '[ùúûűü]', 'y': '[ýÿ]', '-': ' ' };
                for (i in non_asciis) { r = r.replace(new RegExp(non_asciis[i], 'g'), i); }
                return r;
            };

            function LCbrandHTML(brand, i, brands) { // for each brand, create list element with link and fetch image
            
                var brandURL = $(brandsFilter + brand.replace( /\s/g, '') + " a").attr("href"), //get filter url (ex: ../femme?filtre-marques=tnf) / Remove spaces
                    brandTidy = accentsTidy(brand); //remove accents for link and image

                if ( brandURL == undefined ){ // if filter not available (in case of out of stock), return brand landing page url
                    var url = window.location.href, view = "/fr/marques/", regView = /\/fr\//; // find out if FR or EN website and return proper brand page url
                    if ( /\/en\//.test(url) ){
                        view = "/en/brands/";
                    }
                    brandURL = "http://www.lacordee.com" + view + brandTidy;
                }

                var brandHTML = '<li class="item" id="b-' + i + '" ><a href="' + brandURL + '" title="' + brand + '" ><img src="' + mediaURL + brandTidy + '.png" alt="' + brand + ' logo" /><span class"brand-title">' + brand + '</span></a></li>';
                gallery.append(brandHTML); // insert into slider
            }
            
            brands.forEach(LCbrandHTML);
            $(".lc-brand-slider", container).resizeLCBrandSlider(); // resize everything and enable nav

            if (brands.length > 5){ 
                $(".lc-brand-slider", container).initLCBNav();
            }

        });
    };


    // make slider responsive and resize
    $.fn.resizeLCBrandSlider = function(){
        var viewPort = $(this);

        if (viewPort.hasClass("lc-brand-slider") ){
            return this.each(function(){
                var sLenght = $("li.item", this).length,
                    sWidth = $("li.item", this).width(),
                    w = viewPort.width() / 5; // size is 20% of viewPort width
                $("li.item", this).width(w);
                $("ul.gal", this).width(w*sLenght);
            });
        }
    }

    // enable left right navigation
    $.fn.initLCBNav = function(){
        $(".gal-move").css("display", "block");
        $(".gal-move",this).click(function(){

            var slider = $(this).parents(".lc-brand-slider").children("ul.gal").filter(':not(:animated)');

            if ( slider.length != 0 ) { // Check if slider is moving
                var dir = $(this).attr('id').replace("move-next", "-").replace("move-prev", "+"), // move left or right?
                    item = $("li.item", slider), 
                    sLenght = item.length, 
                    sWidth = item.width(),
                    move = dir+"="+ ( ( sWidth * 5) ), // calculate distance to move
                    pos = Math.floor(slider.position().left + (dir + sWidth*5)*1), //predict position left to check if possible
                    min = Math.floor(-(sLenght - 5)*sWidth); // max negative left offset to parent
                
                if ( pos <= 0 && pos >= min ) { // check if predicted position is withing range
                    slider.animate({left : move },400,"linear"); //move normaly
                } if ( pos < min ) { // if predicted pos is smaller than min, 
                    if ( Math.floor(slider.position().left) <= min ){
                        slider.animate({left : 0 },400,"linear"); //move to the end of the list
                    } else {
                        slider.animate({left : min },400,"linear"); // if at the end, go back to begining
                    }   
                } if ( pos > 0 ){
                    slider.animate({left : 0 },400,"linear"); // prevent going the oposite direction or pos zero the prev..
                }
            }
        });
    }

    $(window).resize(function(){
        $(".lc-brand-slider").resizeLCBrandSlider();
    });

}( jQuery ));
